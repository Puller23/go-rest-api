package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.New()
	r.GET("/co-restservice", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "co-restservice",
		})
	})
	r.GET("/co-restservice/actuator/prometheus", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "actuator/prometheus",
		})
	})
	r.Run()
}
